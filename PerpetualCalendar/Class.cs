﻿using System;
/* Copyright (C) 2014-17 Filippo Rigotto.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
       * Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
       * Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.
       * Neither the name of PerpetualCalendar nor the names of its contributors
         may be used to endorse or promote products derived from this software
         without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL Filippo Rigotto BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

namespace PerpetualCalendar
{
    public class PerpetualCalendar
    {
        static int[] mList = new int[] { 0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5 };
        static int[] cList = new int[] { 6, 4, 2, 0 };
        static string[] wDays = new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

        /// <summary>
        /// Get the Week Day from a Date
        /// </summary>
        /// <param name="day">Day between 1 and 31</param>
        /// <param name="month">Month between 1 and 12</param>
        /// <param name="year">Year after 1583</param>
        /// <returns>Day of the week, starting from 0 as Sunday; -1 if data is not valid</returns>
        public static int Evaluate(int day, int month, int year)
        {
            if (day < 1 | day > 31) return -1;
            if (month < 1 | month > 12) return -1;
            if (year <= 1582 && month <= 10 && day < 15) return -1;

            int g, m, a, c;

            g = day % 7;

            m = mList[month - 1];
            if (isLeap(year))
                if (month == 1) m = 6;
                else if (month == 2) m = 2;

            int aa = year % 100 % 28;
            a = aa + (int)(aa / 4);

            c = cList[(int)(year / 100) % 4];

            return (g + m + a + c) % 7;
        }

        /// <summary>
        /// Get the Week Day from a Date
        /// </summary>
        /// <param name="day">Day between 1 and 31</param>
        /// <param name="month">Month between 1 and 12</param>
        /// <param name="year">Year after 1583</param>
        /// <returns>Day of the week as a string (english), or 'Invalid Date'</returns>
        public static string EvaluateString(int day, int month, int year)
        {
            int dow = Evaluate(day, month, year);
            if (dow < 0 && dow > 6) return "Invalid Date";
            return wDays[dow];
        }

        static bool isLeap(int year)
        {
            return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0));
        }
    }
}
